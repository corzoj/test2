package test2;

public class SalariedEmployees implements Employee{
	private double yearly;
	
	public SalariedEmployees(double yearly) {
		this.yearly=yearly;
	}
	
	public double getWeeklyPay() {
		double weeklypay=yearly/52;
		
		return weeklypay;
	}
	
	public double getYearly() {
		return this.yearly;
	}
}
