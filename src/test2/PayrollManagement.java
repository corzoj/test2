package test2;

public class PayrollManagement {

    public static double getTotalExpenses(Employee[] emp) {
        double totalPay = 0;
        
        for(Employee e : emp) {
            totalPay += e.getWeeklyPay();
        }
        
        return totalPay;
    }
    
    public static void main (String[] args) {
        Employee[] emp = new Employee[4];
        emp[0] = new SalariedEmployees(5500);
        emp[1] = new HourlyEmployee(38,20);
        emp[2] = new UnionizedHourlyEmployee(45,10,30,1.5);
        emp[3] = new SalariedEmployees(11000);
        
        System.out.println("Total expenses " + getTotalExpenses(emp));
    }
    
}