package test2;

public class HourlyEmployee implements Employee {
	private double weekHours;
	private double hourly;
	
	public HourlyEmployee(double weekHours, double hourly) {
		this.weekHours=weekHours;
		this.hourly=hourly;
	}
	
	public double getWeeklyPay() {
		double weeklypay;
		weeklypay=weekHours*hourly;
		return weeklypay;
	}
	
	public double getWeekHours() {
		return weekHours;
	}
	
	public double getHourly() {
		return hourly;
	}
}
