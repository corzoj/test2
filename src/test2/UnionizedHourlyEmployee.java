package test2;

public class UnionizedHourlyEmployee extends HourlyEmployee {
	private double maxHoursPerWeek;
	private double overtimeRate;

	public UnionizedHourlyEmployee(double weekHours, double hourly, double maxHoursPerWeek, double overtimeRate) {
		super(weekHours, hourly);
		this.maxHoursPerWeek= maxHoursPerWeek;
		this.overtimeRate=overtimeRate;
	}
	public double getWeeklyPay(){
		double extraTime;
		double totalPay;
		if(super.getWeekHours()<maxHoursPerWeek) {
			totalPay=super.getWeekHours()*super.getHourly();
		}
		else {
			extraTime=super.getWeekHours()-maxHoursPerWeek;
			totalPay=(super.getWeekHours()*super.getHourly())+(extraTime*overtimeRate);
		}
		return totalPay;
	}

}
