package planets;

import java.util.Collection;
import java.util.ArrayList;

public class CollectionMethods {
	public static Collection<Planet> getLargerThan(Collection<Planet> planets, double size){
		ArrayList<Planet> planetsArray= new ArrayList<Planet>();
		
		for(Planet x: planets) {
			if(size<x.getRadius()) {
				planetsArray.add(x);
			}
		}
		return planetsArray;
	}

}
